var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    },
    scale: {
        parent: 'yourgamediv',
        mode: Phaser.Scale.FIT,
        width: 1900,
        height: 950
    }
};

var game = new Phaser.Game(config);

function preload () { 
    this.load.image('player', './assets/player_body.png');
    this.load.image('enemy', './assets/enemy_body.png');
    this.load.image('hand', './assets/player_hands.png');
    this.load.image('background', './assets/background.png');
    this.load.image('player_counter', './assets/player_count_hud.png');
}

function create () {
    //Generating dynamic background
    const BG_WDTH = 512;
    const WORLD_SIZE = 10000;
    
    for (var i = 0; i < WORLD_SIZE / BG_WDTH; i++){
        this.add.tileSprite(i * BG_WDTH, 0, BG_WDTH, BG_WDTH, 'background').setOrigin(0);

        for (var z = 0; z < WORLD_SIZE / BG_WDTH; z++){
            this.add.tileSprite(i * BG_WDTH, z * BG_WDTH, BG_WDTH, BG_WDTH, 'background').setOrigin(0)
        }
    }

    //Setting game and camera bounds
    this.physics.world.setBounds(0, 0, WORLD_SIZE, WORLD_SIZE * 0.95);
    this.cameras.main.setBounds(0, 0, WORLD_SIZE, WORLD_SIZE);

    //Creating game objects
    player = new Player(this, this.physics.add.sprite(450, WORLD_SIZE / 2, 'player'), WORLD_SIZE);
    hud = new HUD(this);

    //Creating camera object
    this.cameras.main.startFollow(player.obj);
    this.cameras.main.followOffset.set(-300, 0);

    //Starting listeners
    player.keyboard_listener(); 
    player.mouse_listener();

    WKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    AKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    SKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
    DKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);

    HUD.create(this);

    //Spawn random enemies
    for (var i = 0; i < 1; i++){
        //var enemy = new Enemy(i + 1, this, this.physics.add.sprite(Math.random() * WORLD_SIZE, Math.random() * WORLD_SIZE, 'enemy'), WORLD_SIZE);
        var enemy = new Enemy(i + 1, this, this.physics.add.sprite(650, WORLD_SIZE / 2, 'enemy'), WORLD_SIZE);
        Manager.entities.push(enemy);
    }

    Manager.entities.push(player);
}

function update() {
    player.update();
    
    HUD.update();

    Manager.entities.forEach(element => {
        if (element instanceof Enemy){
            element.update(Manager.entities);
        }
    }); 
}